﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace WpfUI.Validators
{
    public class DateValidationRule : ValidationRule
    {
        private string? _errorMessage;
        public string? ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new(true, null);

            var stringValue = value as string ?? string.Empty;
            if(!DateTime.TryParse(stringValue, out _))
                return new ValidationResult(false, this.ErrorMessage);

            return result;
        }
    }
}
