﻿using System.Globalization;
using System.Windows.Controls;

namespace WpfUI.Validators
{
    public class IntValueValidationRule : ValidationRule
    {
        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new(true, null);

            var inputString = value == null ? string.Empty : value.ToString();

            if (string.IsNullOrEmpty(inputString))
                return result;
                //return new ValidationResult(true, null);

            if (!int.TryParse(inputString, out _))
                result = new ValidationResult(false, this.ErrorMessage);

            return result;
        }
    }
}
