﻿using AssignmentModels.User;
using System;
using System.ComponentModel;

namespace WpfUI.MVVM
{
    public class UserMVVM : INotifyPropertyChanged, IEquatable<UserMVVM>
    {
        private UserOutput _u;
        private bool _isModified;
        public bool IsModified
        {
            get => _isModified;
            set
            {
                //if (_isModified == value)
                //    return;
                _isModified = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UserMVVM.IsModified)));
            }
        }
        public string? FirstName
        {
            get => _u.FirstName;
            set
            {
                _u.FirstName = value;
                IsModified = true;
            }
        }
        public string? LastName
        {
            get => _u.LastName;
            set
            {
                _u.LastName = value;
                IsModified = true;
            }
        }
        public string? StreetName
        {
            get => _u.StreetName;
            set
            {
                _u.StreetName = value;
                IsModified = true;
            }
        }
        public string? HouseNumber
        {
            get => _u.HouseNumber;
            set
            {
                _u.HouseNumber = value;
                IsModified = true;
            }
        }
        public int? ApartmentNumber
        {
            get => _u.ApartmentNumber;
            set
            {
                _u.ApartmentNumber = value;
                IsModified = true;
            }
        }
        public string? PostalCode
        {
            get => _u.PostalCode;
            set
            {
                _u.PostalCode = value;
                IsModified = true;
            }
        }
        public string? Town
        {
            get => _u.Town;
            set
            {
                _u.Town = value;
                IsModified = true;
            }
        }
        public string? PhoneNumber
        {
            get => _u.PhoneNumber;
            set
            {
                _u.PhoneNumber = value;
                IsModified = true;
            }
        }
        public DateTime? DateOfBirth
        {
            get => _u.DateOfBirth;
            set
            {
                _u.DateOfBirth = value;
                IsModified = true;
            }
        }
        public int? Age
        {
            get => _u.Age;
        }
        public Guid? Id
        {
            get => _u.Id;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public UserMVVM()
        {
            _u = new UserOutput();
        }
        public UserMVVM(UserOutput uo)
        {
            _u = uo;
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            IsModified = true;
        }

        public bool Equals(UserMVVM? other)
        {
            if (other == null)
                return false;
            if (this.FirstName == other.FirstName
                && this.LastName == other.LastName
                && this.StreetName == other.StreetName
                && this.HouseNumber == other.HouseNumber
                && this.ApartmentNumber == other.ApartmentNumber
                && this.Town == other.Town
                && this.PostalCode == other.PostalCode
                && this.PhoneNumber == other.PhoneNumber
                && this.DateOfBirth == other.DateOfBirth)
                return true;
            return false;
        }
    }
}
