﻿using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using WpfUI.Repository;

namespace WpfUI.MVVM
{
    public class MainViewModel : BindableBase, INotifyPropertyChanged
    {
        private IRepository _repository { get; set; }
        private bool _isModified;
        public DelegateCommand CancleButtonClicked { get; private set; }
        public DelegateCommand<object> SaveButtonClicked { get; private set; }
        public ObservableCollection<UserMVVM> ObservableList { get; set; }
        private List<UserMVVM> OriginalList { get; set; }
        public bool IsModified
        {
            get => _isModified;
            set
            {
                _isModified = value;
                OnPropertyChanged(nameof(MainViewModel.IsModified));
            }
        }
        public event PropertyChangedEventHandler? PropertyChanged;
        public MainViewModel(IRepository repository)
        {
            _repository = repository;
            
            ObservableList = new ObservableCollection<UserMVVM>();
            OriginalList = new List<UserMVVM>();

            var users = repository.GetUsers();
            foreach (var user in users)
            {
                var tmpUser = new UserMVVM(user);
                ObservableList.Add(tmpUser);
                OriginalList.Add(tmpUser);
            }

            foreach (var item in ObservableList)
                item.PropertyChanged += Item_PropertyChanged;
            ObservableList.CollectionChanged += List_CollectionChanged;

            CancleButtonClicked = new DelegateCommand(RefreshData);
            SaveButtonClicked = new DelegateCommand<object>(SaveData, CanSaveData);
        }

        private void Item_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            IsModified = true;
        }
        private void List_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IsModified = true;
        }
        public void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private void RefreshData()
        {
            ObservableList.Clear();
            var users = _repository.GetUsers();
            foreach (var user in users)
                ObservableList.Add(new UserMVVM(user));

            foreach (var item in ObservableList)
                item.PropertyChanged += Item_PropertyChanged;
            ObservableList.CollectionChanged += List_CollectionChanged;

            IsModified = false;
        }
        private bool CanSaveData(object user)
        {
            // TODO
            return true;
        }
        private void SaveData(object user)
        {
            var deletedUser = user as UserMVVM;
            var newUsers = GetNewItems();
            var modifiedUsers = GetModifiedItems();

            foreach (var newUser in newUsers)
                _repository.AddUser(newUser);
            foreach (var modifiedUser in modifiedUsers)
                _repository.UpdateUser(modifiedUser.Id!.Value, modifiedUser);
            if(deletedUser != null)
                _repository.DeleteUser(deletedUser.Id!.Value);

            RefreshData();
        }
        #region private
        private List<UserMVVM> GetNewItems()
        {
            return ObservableList.Where(x => x.Id == null).ToList();
        }
        private List<UserMVVM> GetModifiedItems()
        {
            var originalIds = OriginalList.Select(x => x.Id).ToList();
            var modifiedList = ObservableList.Where(x => originalIds.Contains(x.Id) && x.IsModified).ToList();
            return modifiedList;
        }
        #endregion
    }
}
