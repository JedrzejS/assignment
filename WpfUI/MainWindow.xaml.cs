﻿using System.Windows;
using WpfUI.MVVM;
using WpfUI.Repository;

namespace WpfUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IRepository _repository;
        public MainWindow(IRepository repository)
        {
            _repository = repository;

            InitializeComponent();
            DataContext = new MainViewModel(_repository);
        }
    }
}
