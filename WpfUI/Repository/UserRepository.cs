﻿using AssignmentModels.User;
using RestSharp;
using System;
using System.Collections.Generic;
using WpfUI.MVVM;

namespace WpfUI.Repository
{
    public class UserRepository : IRepository
    {
        private RestClient _restClient {get; set;}
        public UserRepository()
        {
            _restClient = new RestClient("https://localhost:7211/");
        }

        public UserOutput GetUser(Guid id)
        {
            var request = new RestRequest("User/GetUser/{id}", Method.Get);
            request.AddUrlSegment("id", id);

            var user = _restClient.GetAsync<UserOutput>(request).Result;

            return user;
        }
        public List<UserOutput> GetUsers()
        {
            var request = new RestRequest("User/GetAllUsers", Method.Get);
            var users = _restClient.GetAsync<List<UserOutput>>(request).Result;

            return users;
        }

        public UserOutput AddUser(UserMVVM input)
        {
            var userInput = ParseMVUserToInput(input);
            var request = new RestRequest("User/AddUser", Method.Post);
            request.AddBody(userInput);

            var user = _restClient.PostAsync<UserOutput>(request).Result;
            return user;
        }

        public void UpdateUser(Guid id, UserMVVM input)
        {
            var userInputUpdate = ParseMVUserToInput(input);

            var request = new RestRequest("User/UpdateUser/{id}", Method.Put);
            request.AddUrlSegment("id", id);
            request.AddBody(userInputUpdate);

            var response = _restClient.PutAsync(request).Result;
        }

        public void DeleteUser(Guid id)
        {
            var request = new RestRequest("User/{id}", Method.Delete);
            request.AddUrlSegment("id", id);

            var response = _restClient.DeleteAsync(request).Result;
        }

        private UserInput ParseMVUserToInput(UserMVVM userInput)
        {
            return new UserInput()
            {
                FirstName = userInput.FirstName,
                LastName = userInput.LastName,
                StreetName = userInput.StreetName,
                HouseNumber = userInput.HouseNumber,
                ApartmentNumber = userInput.ApartmentNumber,
                PostalCode = userInput.PostalCode,
                Town = userInput.Town,
                PhoneNumber = userInput.PhoneNumber,
                DateOfBirth = userInput.DateOfBirth
            };
        }
    }
}