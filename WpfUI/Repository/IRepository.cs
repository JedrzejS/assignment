﻿using AssignmentModels.User;
using System;
using System.Collections.Generic;
using WpfUI.MVVM;

namespace WpfUI.Repository
{
    public interface IRepository
    {
        public List<UserOutput> GetUsers();
        public UserOutput GetUser(Guid id);
        public UserOutput AddUser(UserMVVM input);
        public void UpdateUser(Guid id, UserMVVM input);
        public void DeleteUser(Guid id);
        
    }
}
