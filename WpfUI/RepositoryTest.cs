﻿using AssignmentModels.User;
using System;
using System.Collections.Generic;

namespace WpfUI
{
    public class RepositoryTest
    {
        public List<UserOutput> GetUsers()
        {
            var u1 = new UserOutput()
            {
                Id = Guid.NewGuid(),
                FirstName = "test",
                LastName = "test2",
                StreetName = "test street",
                HouseNumber = "tet house no",
                ApartmentNumber = 1,
                PostalCode = "tetete",
                Town = "town",
                PhoneNumber = "2313214321",
                DateOfBirth = DateTime.Now
            };
            var u2 = new UserOutput()
            {
                Id = Guid.NewGuid(),
                FirstName = "test2",
                LastName = "test3",
                StreetName = "test street22",
                HouseNumber = "tet house no22",
                ApartmentNumber = 132,
                PostalCode = "tetete2",
                Town = "tow213",
                PhoneNumber = "22312311",
                DateOfBirth = DateTime.Now
            };
            return new List<UserOutput>() { u1, u2 };
        }
    }
}
