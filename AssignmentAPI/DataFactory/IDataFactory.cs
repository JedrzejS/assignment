﻿using AssignmentModels.User;
using System.Xml.Linq;

namespace AssignmentAPI.DataFactory
{
    public interface IDataFactory
    {
        public User CreateStoredModel(UserInput inputUser);
        public UserOutput CreateOutputModel(User user);
        public User ParseXmlToUser(XElement element);
    }
}
