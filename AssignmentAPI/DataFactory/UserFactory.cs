﻿using AssignmentModels.User;
using System.Xml.Linq;

namespace AssignmentAPI.DataFactory
{
    public class UserFactory : IDataFactory
    {
        public UserOutput CreateOutputModel(User user)
        {
            var userOutput = new UserOutput()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                StreetName = user.StreetName,
                HouseNumber = user.HouseNumber,
                ApartmentNumber = user.ApartmentNumber,
                PostalCode = user.PostalCode,
                Town = user.Town,
                PhoneNumber = user.PhoneNumber,
                DateOfBirth = user.DateOfBirth
            };
            return userOutput;
        }

        public User CreateStoredModel(UserInput inputUser)
        {
            var user = new User
            {
                FirstName = inputUser.FirstName,
                LastName = inputUser.LastName,
                StreetName = inputUser.StreetName,
                HouseNumber = inputUser.HouseNumber,
                ApartmentNumber = inputUser.ApartmentNumber,
                PostalCode = inputUser.PostalCode,
                Town = inputUser.Town,
                PhoneNumber = inputUser.PhoneNumber,
                DateOfBirth = inputUser.DateOfBirth
            };
            return user;
        }
        public User ParseXmlToUser(XElement element)
        {
            var user = new User()
            {
                Id = Guid.Parse(element.Attribute(nameof(User.Id))!.Value),
                FirstName = element.Element(nameof(User.FirstName))?.Value,
                LastName = element.Element(nameof(User.LastName))?.Value,
                StreetName = element.Element(nameof(User.StreetName))?.Value,
                HouseNumber = element.Element(nameof(User.HouseNumber))?.Value,
                ApartmentNumber = int.TryParse(element.Element(nameof(User.ApartmentNumber))?.Value, out int tmpAp) == true ? tmpAp : null,
                PostalCode = element.Element(nameof(User.PostalCode))?.Value,
                Town = element.Element(nameof(User.Town))?.Value,
                PhoneNumber = element.Element(nameof(User.PhoneNumber))?.Value,
                DateOfBirth = DateTime.TryParse(element.Element(nameof(User.DateOfBirth))?.Value, out DateTime tmpBirth) == true ? tmpBirth : null,
            };
            return user;
        }
    }
}
