using AssignmentAPI.Configuration;
using AssignmentAPI.DataFactory;
using AssignmentAPI.DataHandler;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IDataHandler, XmlDataHandler>();
builder.Services.AddSingleton<IConfigHandler, JsonConfigHandler>();
builder.Services.AddScoped<IDataFactory, UserFactory>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();
