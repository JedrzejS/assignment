﻿using AssignmentAPI.DataFactory;
using AssignmentAPI.DataHandler;
using AssignmentModels.User;
using Microsoft.AspNetCore.Mvc;

namespace AssignmentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        #region DI
        private IDataHandler _dataHandler { get; set; }
        private IDataFactory _factory { get; set; }
        public UserController(IDataHandler dataHandler, IDataFactory factory)
        {
            _dataHandler = dataHandler;
            _factory = factory;
        }
        #endregion

        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetAll()
        {
            var allUsers = await _dataHandler.GetUsersAsync();
            var allUsersOutput = allUsers.Select(x => _factory.CreateOutputModel(x)).ToList();
            return Ok(allUsersOutput);
        }

        [HttpGet("GetUser/{id}")]
        public async Task<IActionResult> GetUserById(Guid id)
        {
            var user = await _dataHandler.GetUserByIdAsync(id);
            if (user == null)
                return NotFound();

            return Ok(_factory.CreateOutputModel(user));
        }

        [HttpPost("AddUser")]
        public async Task<IActionResult> AddUser(UserInput newUser)
        {
            var createdUser = await _dataHandler.AddUserAsync(newUser);
            if (createdUser == null)
                return BadRequest();
            return CreatedAtAction(nameof(GetUserById), new { id = createdUser.Id }, createdUser);
        }

        [HttpPut("UpdateUser/{id}")]
        public async Task<IActionResult> UpdateUser(Guid id, [FromBody] UserInput userToUpdate)
        {
            var updated = await _dataHandler.UpdateUserAsync(id, userToUpdate);
            if (!updated)
                return NotFound();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(Guid id)
        {   
            var deleted = await _dataHandler.DeleteUserAsync(id);
            if(!deleted)
                return NotFound();
            return Ok();
        }
    }
}
