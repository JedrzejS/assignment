﻿namespace AssignmentAPI.Configuration
{
    public class JsonConfigHandler : IConfigHandler
    {
        public AppSettings GetConfig()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            return config.GetSection(nameof(AppSettings)).Get<AppSettings>();
        }
    }
}
