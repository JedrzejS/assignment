﻿namespace AssignmentAPI.Configuration
{
    public interface IConfigHandler
    {
        public AppSettings GetConfig();
    }
}
