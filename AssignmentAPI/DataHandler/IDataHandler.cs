﻿using AssignmentModels.User;

namespace AssignmentAPI.DataHandler
{
    public interface IDataHandler
    {
        public Task<IEnumerable<User>> GetUsersAsync();
        public Task<User?> GetUserByIdAsync(Guid id);
        public Task<User?> AddUserAsync(UserInput user);
        public Task<bool> UpdateUserAsync(Guid id, UserInput user); 
        public Task<bool> DeleteUserAsync(Guid id);
    }
}
