﻿using AssignmentAPI.Configuration;
using AssignmentAPI.DataFactory;
using AssignmentModels.User;
using System.Xml;
using System.Xml.Linq;

namespace AssignmentAPI.DataHandler
{
    public class XmlDataHandler : IDataHandler
    {
        #region DI
        private readonly IConfigHandler _config;
        private readonly IDataFactory _factory;
        public XmlDataHandler(IConfigHandler config, IDataFactory factory)
        {
            _config = config;
            _factory = factory;
        }
        #endregion
        #region public
        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            var xmlFilePath = _config.GetConfig().XmlFilePath;

            var doc = await GetOrCreateXmlFileAsync(xmlFilePath);
            var retUsers = doc.Descendants("user").Select(x => _factory.ParseXmlToUser(x)).ToList();
            return await Task.FromResult(retUsers);
        }
        public async Task<User?> GetUserByIdAsync(Guid id)
        {
            var xmlFilePath = _config.GetConfig().XmlFilePath;
            var doc = await GetOrCreateXmlFileAsync(xmlFilePath);

            var retUser = doc.Descendants("user")
                .Where(x => x.Attribute(nameof(User.Id))?.Value == id.ToString())
                .Select(x => _factory.ParseXmlToUser(x))
                .SingleOrDefault();

            return retUser;
        }
        public async Task<User?> AddUserAsync(UserInput user)
        {
            var xmlFilePath = _config.GetConfig().XmlFilePath;

            var doc = await GetOrCreateXmlFileAsync(xmlFilePath);

            User userToStore = _factory.CreateStoredModel(user);

            doc.Element("users")?.Add(new XElement("user",
                new XAttribute(nameof(userToStore.Id), userToStore.Id),
                new XElement(nameof(userToStore.FirstName), userToStore.FirstName),
                new XElement(nameof(userToStore.LastName), userToStore.LastName),
                new XElement(nameof(userToStore.StreetName), userToStore.StreetName),
                new XElement(nameof(userToStore.HouseNumber), userToStore.HouseNumber),
                new XElement(nameof(userToStore.ApartmentNumber), userToStore.ApartmentNumber.HasValue ? user.ApartmentNumber.Value : string.Empty),
                new XElement(nameof(userToStore.PostalCode), userToStore.PostalCode),
                new XElement(nameof(userToStore.Town), userToStore.Town),
                new XElement(nameof(userToStore.PhoneNumber), userToStore.PhoneNumber),
                new XElement(nameof(userToStore.DateOfBirth), userToStore.DateOfBirth.HasValue ? user.DateOfBirth.Value.Date : string.Empty)));
            await SaveDocAsync(doc, xmlFilePath);

            return await Task.FromResult(userToStore);
        }

        public async Task<bool> UpdateUserAsync(Guid id, UserInput user)
        {
            var xmlFilePath = _config.GetConfig().XmlFilePath;
            var doc = await GetOrCreateXmlFileAsync(xmlFilePath);

            var retUser = doc.Descendants("user")
                .Where(x => x.Attribute(nameof(User.Id))?.Value == id.ToString())
                .SingleOrDefault();

            if (retUser != null)
            {
                IEnumerable<XElement> userElements = retUser.Descendants();
                foreach (var element in userElements)
                {
                    var propValue = GetPropValue(user, element.Name.LocalName);
                    if (propValue != null && !string.IsNullOrEmpty(propValue.ToString()))
                        element.Value = propValue.ToString().Trim();
                };
                await SaveDocAsync(doc, xmlFilePath);

                return await Task.FromResult(true);
            }
            return await Task.FromResult(false);
        }

        public async Task<bool> DeleteUserAsync(Guid id)
        {
            var xmlFilePath = _config.GetConfig().XmlFilePath;
            var doc = await GetOrCreateXmlFileAsync(xmlFilePath);

            var elements = doc.Descendants("user").Where(x => x.Attribute(nameof(User.Id))?.Value == id.ToString());
            
            if(!elements.Any())
                return await Task.FromResult(false);
            
            elements.Remove();
            await SaveDocAsync(doc, xmlFilePath);

            return await Task.FromResult(true);
        }
        #endregion
        #region private
        private static async Task<XDocument> GetOrCreateXmlFileAsync(string filePath)
        {
            if (!File.Exists(filePath))
            {
                XDocument doc = new(new XElement("users"));
                doc.Save(filePath);
            }
            XmlReaderSettings settings = new()
            {
                Async = true
            };
            using XmlReader reader = XmlReader.Create(filePath, settings);

            return await XDocument.LoadAsync(reader, LoadOptions.None, new CancellationToken());
        }
        private static async Task SaveDocAsync(XDocument doc, string filePath)
        {
            XmlWriterSettings writerSettings = new() { Async = true };
            using var xmlWriter = XmlWriter.Create(filePath, writerSettings);
            await doc.SaveAsync(xmlWriter, new CancellationToken());
        }
        private static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }
        #endregion
    }
}
