﻿namespace AssignmentModels.User
{
    public class User : User_Base
    {
        public Guid Id { get; set; }

        public User()
        {
            Id = Guid.NewGuid();
            FirstName = string.Empty;
            LastName = string.Empty;
            StreetName = string.Empty;
            HouseNumber = string.Empty;
            ApartmentNumber = null;
            PostalCode = string.Empty;
            Town = string.Empty;
            PhoneNumber = string.Empty;
            DateOfBirth = null;
        }
    }
}