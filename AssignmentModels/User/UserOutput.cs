﻿namespace AssignmentModels.User
{
    public class UserOutput : User_Base
    {
        public Guid? Id { get; set; }
        public int Age 
        {
            get
            {
                if (!DateOfBirth.HasValue)
                    return 0;
                else
                {
                    int age = DateTime.Now.Year - DateOfBirth.Value.Date.Year;
                    if (DateOfBirth?.Date > DateTime.Now.AddYears(-age))
                        age--;
                    return age;
                }
            }
        }
    }
}
