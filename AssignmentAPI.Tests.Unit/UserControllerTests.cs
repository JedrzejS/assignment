﻿using AssignmentAPI.Controllers;
using AssignmentAPI.DataFactory;
using AssignmentAPI.DataHandler;
using AssignmentModels.User;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using System;
using NSubstitute.ReturnsExtensions;

namespace AssignmentAPI.Tests.Unit
{
    public class UserControllerTests
    {
        #region Setup

        private readonly UserController _sut;
        private readonly IDataHandler _dataHandler = Substitute.For<IDataHandler>();
        private readonly IDataFactory _dataFactory = Substitute.For<IDataFactory>();
        public UserControllerTests()
        {
            _sut = new UserController(_dataHandler, _dataFactory);
        }
        #endregion

        #region GetAll
        [Fact]
        public async Task GetAll_ShouldReturnEmptyList_WhenNoUsersExist()
        {
            // Arrange
            _dataHandler.GetUsersAsync().Returns(Enumerable.Empty<User>());

            // Act
            var result = (OkObjectResult)await _sut.GetAll();

            // Assert
            result.StatusCode.Should().Be(200);
            result.Value.As<IEnumerable<UserOutput>>().Should().BeEmpty();
        }
        [Fact]
        public async Task GetAll_ShouldReturnUsersOutput_WhenUsersExist()
        {
            //Arrange
            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                StreetName = "Test Street Name",
                HouseNumber = "Test House Number",
                ApartmentNumber = 222,
                PostalCode = "Test Postal Code",
                Town = "Test Town",
                PhoneNumber = "+44 3122 321",
                DateOfBirth = DateTime.UtcNow
            };
            var users = new[] { user };
            var usersOutpout = users.Select(x => _dataFactory.CreateOutputModel(x));
            _dataHandler.GetUsersAsync().Returns(users);

            //Act
            var result = (OkObjectResult)await _sut.GetAll();

            //Assert
            result.StatusCode.Should().Be(200);
            result.Value.As<IEnumerable<UserOutput>>().Should().BeEquivalentTo(usersOutpout);
        }
        #endregion

        #region GetUserById
        [Fact]
        public async Task GetUserById_ReturnOkAndObject_WhenUserExists()
        {
            // Arrange
            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                StreetName = "Test Street Name",
                HouseNumber = "Test House Number",
                ApartmentNumber = 222,
                PostalCode = "Test Postal Code",
                Town = "Test Town",
                PhoneNumber = "+44 3122 321",
                DateOfBirth = DateTime.UtcNow
            };
            _dataHandler.GetUserByIdAsync(user.Id).Returns(user);
            var userOutput = _dataFactory.CreateOutputModel(user);

            // Act
            var result = (OkObjectResult)await _sut.GetUserById(user.Id);

            // Assert
            result.StatusCode.Should().Be(200);
            result.Value.Should().BeEquivalentTo(userOutput);
        }

        [Fact]
        public async Task GetUserById_ReturnNotFound_WhenUserDoesntExists()
        {
            // Arrange
            _dataHandler.GetUserByIdAsync(Arg.Any<Guid>()).ReturnsNull();

            // Act
            var result = (NotFoundResult)await _sut.GetUserById(Guid.NewGuid());

            // Assert
            result.StatusCode.Should().Be(404);
        }
        #endregion

        #region AddUser
        [Fact]
        public async Task AddUser_ShouldReturnUserOutput_WhenRequestIsValid()
        {
            // Arrange
            var userInput = new UserInput
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                StreetName = "Test Street Name",
                HouseNumber = "Test House Number",
                ApartmentNumber = 222,
                PostalCode = "Test Postal Code",
                Town = "Test Town",
                PhoneNumber = "+44 3122 321",
                DateOfBirth = DateTime.UtcNow
            };
            var user = new User
            {
                Id = Guid.NewGuid(),
                FirstName = userInput.FirstName,
                LastName = userInput.LastName,
                StreetName = userInput.StreetName,
                HouseNumber = userInput.HouseNumber,
                ApartmentNumber = userInput.ApartmentNumber,
                PostalCode = userInput.PostalCode,
                Town = userInput.Town,
                PhoneNumber = userInput.PhoneNumber,
                DateOfBirth = userInput.DateOfBirth
            };

            _dataHandler.AddUserAsync(Arg.Do<UserInput>(x => userInput = x)).Returns(user);

            // Act
            var result = (CreatedAtActionResult)await _sut.AddUser(userInput);

            // Assert
            var expectedUserResponse = _dataFactory.CreateStoredModel(userInput);
            result.StatusCode.Should().Be(201);
            result.Value.As<UserOutput>().Should().BeEquivalentTo(expectedUserResponse);
            result.RouteValues!["id"].Should().BeEquivalentTo(user.Id);
        }
        [Fact]
        public async Task AddUser_ShouldReturnBadRequest_WhenUserCantBeCreated()
        {
            //Arrange
            _dataHandler.AddUserAsync(Arg.Any<UserInput>()).ReturnsNull();

            //Act
            var result = (BadRequestResult)await _sut.AddUser(new UserInput());

            //Assert
            result.StatusCode.Should().Be(400);
        }
        #endregion

        #region UpdateUser
        [Fact]
        public async Task UpdateUser_ShouldReturnNoContent_WhenUserUpdated()
        {
            //Arrange
            var userInput = new UserInput
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                StreetName = "Test Street Name",
                HouseNumber = "Test House Number",
                ApartmentNumber = 222,
                PostalCode = "Test Postal Code",
                Town = "Test Town",
                PhoneNumber = "+44 3122 321",
                DateOfBirth = DateTime.UtcNow
            };
            var guid = Guid.NewGuid();
            _dataHandler.UpdateUserAsync(guid, userInput).Returns(true);

            //Act
            var result = (NoContentResult)await _sut.UpdateUser(guid, userInput);

            //Assert
            result.StatusCode.Should().Be(204);
        }
        [Fact]
        public async Task UpdateUser_ShouldReturnNotFound_WhenNoUserToUpdate()
        {
            //Arrange
            _dataHandler.UpdateUserAsync(Arg.Any<Guid>(), Arg.Any<UserInput>()).Returns(false);

            //Act
            var result = (NotFoundResult)await _sut.UpdateUser(Guid.NewGuid(), new UserInput());

            //Assert
            result.StatusCode.Should().Be(404);
        }
        #endregion

        #region DeleteUser
        [Fact]
        public async Task DeleteUser_ShouldReturnOk_WhenUserDeleted()
        {
            //Arrange
            _dataHandler.DeleteUserAsync(Arg.Any<Guid>()).Returns(true);

            //Act
            var result = (OkResult)await _sut.DeleteUser(Guid.NewGuid());

            //Assert
            result.StatusCode.Should().Be(200);
        }
        [Fact]
        public async Task DeleteUser_ShouldReturnNotFound_WhenWrongGuidProvided()
        {
            //Arrange
            _dataHandler.DeleteUserAsync(Arg.Any<Guid>()).Returns(false);

            //Act
            var result = (NotFoundResult) await _sut.DeleteUser(Guid.NewGuid());

            //Assert
            result.StatusCode.Should().Be(404);
        }
        #endregion
    }
}
