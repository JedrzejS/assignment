Solution for the recruitment assignment.

----------------------------------------

Solution contains separate projects for REST API, API tests, shered models and UI written using WPF.
UI project was my first encounter with WPF. I tried to implement the MVVM pattern with rather mixed results, however it is functional. 

Given my current knowledge I, most probably, would choose Blazor as my UI technology.
